
package za.co.annotations.example;

import za.co.annotations.example.Test;
import za.co.annotations.example.TesterInfo;
import za.co.annotations.example.TesterInfo.Priority;


/*

#3. 

Unit Test Example

Create a simple unit test example, and annotated 
with the new custom annotations – @Test and @TesterInfo.

*/


@TesterInfo(
	priority = Priority.HIGH, 
	createdBy = "mkyong.com",  
	tags = {"sales","test" }
)
public class TestExample {

	@Test
	void testA() {
	  if (true)
		throw new RuntimeException("This test always failed");
	}

	@Test(enabled = false)
	void testB() {
	  if (false)
		throw new RuntimeException("This test always passed");
	}

	@Test(enabled = true)
	void testC() {
	  if (10 > 1) {
		// do nothing, this test always passed.
	  }
	}

}
