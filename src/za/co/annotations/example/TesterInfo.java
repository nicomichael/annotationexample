package za.co.annotations.example;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/*

#2

@TesterInfo Annotation

This @TesterInfo is applied on class level, store the tester details. 
This shows the different use of return types – enum, array and string.

*/

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE) //on class level
public @interface TesterInfo {

	public enum Priority {
	   LOW, MEDIUM, HIGH
	}

	Priority priority() default Priority.MEDIUM;
	
	String[] tags() default "";
	
	String createdBy() default "Joe  Soap";
	
	String lastModified() default "12/08/2015";    
}
