package za.co.annotations.example;
/*

#1

@Test Annotation

This @interface tells Java this is a custom annotation. Later, 
you can annotate it on method level like this @Test(enable=false)

Method declarations must not have any parameters or a throws clause. 
Return types are restricted to primitives, String, Class, enums, annotations, 
and arrays of the preceding types.


*/

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD) //can use in method only.
public @interface Test {
	
	//should ignore this test?
	public boolean enabled() default true;
	
}
